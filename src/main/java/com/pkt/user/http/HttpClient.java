package com.pkt.user.http;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.pkt.user.kafka.Greeting;

@Service
public class HttpClient {

	public Greeting postData(String url, Greeting msg) {
		RestTemplate restTemplate = new RestTemplate();
		// Http header
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		// request entity is created with request headers
		HttpEntity<Greeting> requestEntity = new HttpEntity<>(msg, requestHeaders);
		ResponseEntity<Greeting> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				Greeting.class);

		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			System.out.println("response received" + responseEntity.getBody());
			return responseEntity.getBody();
		} else {
			System.out.println("error occurred");
			System.out.println(responseEntity.getStatusCode());
			return null;
		}

	}

}
