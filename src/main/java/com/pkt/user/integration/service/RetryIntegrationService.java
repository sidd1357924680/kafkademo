package com.pkt.user.integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.pkt.user.http.HttpClient;
import com.pkt.user.kafka.Greeting;
import com.pkt.user.kafka.MessageProducer;

@Service
public class RetryIntegrationService {

	@Value(value = "${retry1.topic.name}")
	private String retry1TopicName;
	@Value(value = "${retry2.topic.name}")
	private String retry2TopicName;
	@Value(value = "${retry3.topic.name}")
	private String retry3TopicName;
	@Value(value = "${retryDead.topic.name}")
	private String retryDeadTopicName;

	@Autowired
	MessageProducer producer;

	@Autowired
	HttpClient httpClient;

	@Value(value = "${url}")
	private String url;
	@Value(value = "${url2}")
	private String url2;

	public void retry1(Greeting greeting) throws InterruptedException {
		producer.sendGreetingMessage(greeting, retry1TopicName);
	}

	@KafkaListener(topics = "${retry1.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
	public void retry1Listener(Greeting greeting) {
		System.out.println("==========================");
		System.out.println("Recieved retry1 message: " + greeting);
		Greeting g = httpClient.postData(url, greeting);
		if (g == null) {
			producer.sendGreetingMessage(greeting, retry2TopicName);
			System.out.println(" Retry2:");
		}
	}

	@KafkaListener(topics = "${retry2.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
	public void retry2Listener(Greeting greeting) {
		System.out.println("==========================");
		System.out.println("Recieved retry2 message: " + greeting);
		Greeting g = httpClient.postData(url, greeting);
		if (g == null) {
			producer.sendGreetingMessage(greeting, retry3TopicName);
			System.out.println(" Retry3:");
		}

	}

	@KafkaListener(topics = "${retry3.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
	public void retry3Listener(Greeting greeting) {
		System.out.println("==========================");
		System.out.println("Recieved retry3 message: " + greeting);
		Greeting g = httpClient.postData(url2, greeting);
		if (g == null) {
			producer.sendGreetingMessage(greeting, retryDeadTopicName);
			System.out.println(" Dead:");
		}
	}

	@KafkaListener(topics = "${retryDead.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
	public void retryDeadListener(Greeting greeting) {
		System.out.println("==========================");
		System.out.println("Recieved retryDead message: " + greeting);
	}

}
