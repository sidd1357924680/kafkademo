package com.pkt.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pkt.user.integration.service.RetryIntegrationService;
import com.pkt.user.kafka.Greeting;

@Service
public class RetryService {

	@Autowired
	RetryIntegrationService retryIntegrationService;

	public void sendMessage(Greeting greeting) throws InterruptedException {
		retryIntegrationService.retry1(greeting);
	}

}
