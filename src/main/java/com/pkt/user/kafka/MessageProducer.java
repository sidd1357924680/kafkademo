package com.pkt.user.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class MessageProducer {

	@Autowired
	private KafkaTemplate<String, Greeting> greetingKafkaTemplate;

	@Value(value = "${greeting.topic.name}")
	private String greetingTopicName;

	public void sendGreetingMessage(Greeting message ,String topicName) {

		ListenableFuture<SendResult<String, Greeting>> future = greetingKafkaTemplate.send(topicName, message);

		future.addCallback(new ListenableFutureCallback<SendResult<String, Greeting>>() {

			@Override
			public void onSuccess(SendResult<String, Greeting> result) {
				System.out.println("==========================");
				System.out.println(
						"Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("==========================");
				System.out.println("Unable to send message=[" + message + "] due to : " + ex.getMessage());
			}
		});
	}
}
