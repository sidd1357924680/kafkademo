package com.pkt.user.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class KafkaTopicConfig {

	@Value(value = "${kafka.bootstrapAddress}")
	private String bootstrapAddress;

	@Value(value = "${greeting.topic.name}")
	private String greetingTopicName;

	@Value(value = "${retry1.topic.name}")
	private String retry1TopicName;
	
	@Value(value = "${retry2.topic.name}")
	private String retry2TopicName;
	
	@Value(value = "${retry3.topic.name}")
	private String retry3TopicName;
	
	@Value(value = "${retryDead.topic.name}")
	private String retryDeadTopicName;

	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		return new KafkaAdmin(configs);
	}

	@Bean
	public NewTopic topic4() {
		return new NewTopic(greetingTopicName, 1, (short) 1);
	}
	// new one 
	@Bean
	public NewTopic topic5() {
		return new NewTopic(retry1TopicName, 1, (short) 1);
	}
	@Bean
	public NewTopic topic8() {
		return new NewTopic(retry2TopicName, 1, (short) 1);
	}
	@Bean
	public NewTopic topic6() {
		return new NewTopic(retry3TopicName, 1, (short) 1);
	}
	@Bean
	public NewTopic topic7() {
		return new NewTopic(retryDeadTopicName, 1, (short) 1);
	}
}
