package com.pkt.user.kafka;

import java.util.concurrent.CountDownLatch;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class MessageListener {
	public CountDownLatch latch = new CountDownLatch(3);

	public CountDownLatch partitionLatch = new CountDownLatch(2);

	public CountDownLatch filterLatch = new CountDownLatch(2);

	public CountDownLatch greetingLatch = new CountDownLatch(1);

	@KafkaListener(topics = "${greeting.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
	public void greetingListener(Greeting greeting) {
		System.out.println("==========================");
		System.out.println("Recieved greeting message: " + greeting);
		this.greetingLatch.countDown();
	}

}
