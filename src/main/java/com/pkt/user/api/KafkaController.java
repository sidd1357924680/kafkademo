package com.pkt.user.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pkt.user.kafka.Greeting;
import com.pkt.user.service.RetryService;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {

	@Autowired
	private RetryService retryService;

	@PostMapping(value = "/publish")
	public void sendMessage(@RequestBody Greeting greeting) throws InterruptedException {
		retryService.sendMessage(greeting);
	}
	@PostMapping(value = "/api")
	public Greeting postMessage(@RequestBody Greeting greeting) throws InterruptedException {
		return null;
	}
	@PostMapping(value = "/apiSuccess")
	public Greeting postMessageSuccess(@RequestBody Greeting greeting) throws InterruptedException {
		return greeting;
	}
}
